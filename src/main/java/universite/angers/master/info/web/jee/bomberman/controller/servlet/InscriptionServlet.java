package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.controller.dao.UserDAO;
import universite.angers.master.info.web.jee.bomberman.controller.form.InscriptionForm;
import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * Servlet qui permet d'inscrire un utilisateur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/InscriptionServlet")
public class InscriptionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(InscriptionServlet.class);

	private static final String ATT_USER = "user";
	private static final String ATT_FORM = "form";
	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";
	private static final String VUE = "/WEB-INF/jsp/inscription.jsp";
	private static final String VUE_HOME = "/WEB-INF/jsp/home.jsp";

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Affichage de la page d'inscription */
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Préparation de l'objet formulaire */

		/*
		 * Appel au traitement et à la validation de la requête, et récupération du bean
		 * en résultant
		 */
		
		InscriptionForm form = new InscriptionForm();
		LOG.debug("Form inscription : " + form);

		User user = form.registerUser(request);
		LOG.debug("User : " + user);

		if (form.getErreurs().isEmpty()) {

			if (UserDAO.getInstance().getUserByEmail(user.getEmail()) != null) {
				form.setResult("Email déja utilisé");
				LOG.debug("-----deja existant");
			}

			if (UserDAO.getInstance().getUserByPseudo(user.getPseudo()) != null) {
				form.setResult("Pseudo déja utilisé");
				LOG.debug("-----deja existant");
			}
		}

		/* Stockage du formulaire et du bean dans l'objet request */
		LOG.debug("Enregistrement du formulaire et du user dans la requete");
		
		//Quoi qu'il en soit on enregistre le formulaire et l'utilisateur
		request.setAttribute(ATT_FORM, form);
		request.setAttribute(ATT_USER, user);
		
		//Si l'utilisateur n'est pas dans la BD
		if (form.getErreurs().isEmpty() && UserDAO.getInstance().getUserByEmail(user.getEmail()) == null
				&& UserDAO.getInstance().getUserByPseudo(user.getPseudo()) == null) {

			//On crée le compte et on associe l'utilisateur
			Account account = new Account();
			account.setUser(user);
			LOG.debug("Account : " + account);

			boolean createdAccount = AccountDAO.getInstance().create(account);
			LOG.debug("Compte crée : " + createdAccount);
			
			//Si création compte réussi
			if (createdAccount) {
				form.setResult("Création du compte réussie");
				LOG.debug("Création du compte réussie");

				//On enregistre le compte dans la requete
				request.setAttribute(ATT_SESSION_ACCOUNT, account);
				
				this.getServletContext().getRequestDispatcher(VUE_HOME).forward(request, response);

			} else {
				form.setResult("Création du compte échouée");
				LOG.debug("Création du compte échouée");
				
				this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
			}
		//Si création utilisateur échouée
		} else {
			LOG.debug("Utilisateur non enregistré !");
			
			this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
		}
	}
}
