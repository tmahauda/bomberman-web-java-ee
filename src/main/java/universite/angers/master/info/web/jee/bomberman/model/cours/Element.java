package universite.angers.master.info.web.jee.bomberman.model.cours;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table
public class Element {

	@PrimaryKey
	private String id;
	
	@Column
	private String pid;
	
	@Column
	private int level;
	
	@Column
	private int position;
	
	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column
	private boolean expanded;
	
	@Column
	private boolean selected;
	
	@ForeignKey
	private List<Element> childrens;
	
	@Column
	private boolean hasChildren;

	public Element() {
		this.id = UUID.randomUUID().toString();
		this.childrens = new ArrayList<>();
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the pid
	 */
	public String getPid() {
		return pid;
	}

	/**
	 * @param pid the pid to set
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the expanded
	 */
	public boolean isExpanded() {
		return expanded;
	}

	/**
	 * @param expanded the expanded to set
	 */
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return the childrens
	 */
	public List<Element> getChildrens() {
		return childrens;
	}

	/**
	 * @param childrens the childrens to set
	 */
	public void setChildrens(List<Element> childrens) {
		this.childrens = childrens;
	}

	/**
	 * @return the hasChildren
	 */
	public boolean isHasChildren() {
		return hasChildren;
	}

	/**
	 * @param hasChildren the hasChildren to set
	 */
	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((childrens == null) ? 0 : childrens.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (expanded ? 1231 : 1237);
		result = prime * result + (hasChildren ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + level;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pid == null) ? 0 : pid.hashCode());
		result = prime * result + position;
		result = prime * result + (selected ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Element other = (Element) obj;
		if (childrens == null) {
			if (other.childrens != null)
				return false;
		} else if (!childrens.equals(other.childrens))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (expanded != other.expanded)
			return false;
		if (hasChildren != other.hasChildren)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (level != other.level)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pid == null) {
			if (other.pid != null)
				return false;
		} else if (!pid.equals(other.pid))
			return false;
		if (position != other.position)
			return false;
		if (selected != other.selected)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Element [id=" + id + ", pid=" + pid + ", level=" + level + ", position=" + position + ", name=" + name
				+ ", description=" + description + ", expanded=" + expanded + ", selected=" + selected + ", childrens="
				+ childrens + ", hasChildren=" + hasChildren + "]";
	}
}
