package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * Servlet qui permet de déconnecter un utilisateur et de le redériger vers la
 * page d'accueil
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/DeconnexionServlet")
public class DeconnexionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(DeconnexionServlet.class);

	private static final String URL_REDIRECTION = "http://localhost:8080/project-web-jee-bomberman/HomeServlet";

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération et destruction de la session en cours */
		HttpSession session = request.getSession();
		session.invalidate();

		LOG.debug("Session : " + session);

		/* Redirection vers la page d'accueil ! */
		try {
			response.sendRedirect(URL_REDIRECTION);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
}