package universite.angers.master.info.web.jee.bomberman.controller.dao;

import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.web.jee.bomberman.model.cours.Structure;

/**
 * DAO Account
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class StructureDAO extends DAO<Structure> {

	private static StructureDAO structDAO;

	private StructureDAO() {
		super(FactorySQLite.getInstance(), Structure.class);
	}

	public static synchronized StructureDAO getInstance() {
		if (structDAO == null)
			structDAO = new StructureDAO();

		return structDAO;
	}
}
