package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet qui permet de télécharger le jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(DownloadServlet.class);

	private static final String NAME_CLIENT = "project-client-bomberman-0.0.1-SNAPSHOT-jar-with-dependencies.jar";
	private static final String PATH_CLIENT = "WEB-INF/lib/";
	private static final int ARBITARY_SIZE = 1048;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// avec cette solution on télécharge le fichier jar
		response.setContentType("application/jar");
		response.setHeader("Content-disposition", "attachment; filename=Bomberman.jar");

		try (InputStream in = request.getServletContext().getResourceAsStream(PATH_CLIENT + NAME_CLIENT);
				OutputStream out = response.getOutputStream()) {

			byte[] buffer = new byte[ARBITARY_SIZE];

			int numBytesRead = 0;
			try {
				while ((numBytesRead = in.read(buffer)) > 0) {
					out.write(buffer, 0, numBytesRead);
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}
}
