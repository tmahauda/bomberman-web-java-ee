package universite.angers.master.info.web.jee.bomberman.controller.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * Servlet Filter implementation class WebFilter
 */
@javax.servlet.annotation.WebFilter("/*")
public class WebFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(WebFilter.class);
	private static final String VUE = "/WEB-INF/jsp/home.jsp";
	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		
		Account account = (Account) session.getAttribute(ATT_SESSION_ACCOUNT);
		LOG.debug("Filter account : " + account);
		
		String path = req.getServletPath();
		LOG.debug("Filter path : " + path);
		
		//Si l'utilisateur est bien connecté ou que certaines pages sont accessibles sans compte
		if(account != null || path.startsWith("/HomeServlet") || path.startsWith("/DeconnexionServlet")
				|| path.startsWith("/ConnexionServlet") || path.startsWith("/InscriptionServlet")
				|| path.startsWith("/imports") || path.startsWith("/Semantic")
				|| path.startsWith("/api")) {
			//Affichage de la page restreinte
			chain.doFilter(request, response);	
		} else {
			//Redirection vers la page publique
			request.getRequestDispatcher(VUE).forward(request, response);
		}
	}
}
