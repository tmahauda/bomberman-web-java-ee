package universite.angers.master.info.web.jee.bomberman.controller.api.servlet;

import javax.servlet.annotation.WebServlet;
import universite.angers.master.info.web.jee.bomberman.controller.api.CoursAPI;
import universite.angers.master.info.web.jee.bomberman.model.cours.Cours;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet(CoursAPIRestServlet.URL)
public class CoursAPIRestServlet extends APIRestServlet<Cours> {

	private static final long serialVersionUID = 1L;
	protected static final String URL = "/api/cours/*";

	public CoursAPIRestServlet() {
		super(CoursAPI.getInstance());
	}
}