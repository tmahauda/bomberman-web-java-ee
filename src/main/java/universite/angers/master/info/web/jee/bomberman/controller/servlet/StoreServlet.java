package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.store.Item;
import universite.angers.master.info.web.jee.bomberman.model.store.Store;
import universite.angers.master.info.web.jee.bomberman.model.store.StoreFactory;

/**
 * Servlet qui permet d'afficher le magasin
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/StoreServlet")
public class StoreServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(StoreServlet.class);

	private static final String VUE = "/WEB-INF/jsp/store.jsp";
	private static final String ATT_STORE = "store";
	private static final String ATT_ITEM = "item";
	private static final String ATT_ISPOSSIBLETOACESS = "isPossibleToAccess";	
	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StoreServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//On récupère le magasin
		Store store = StoreFactory.getInstance();
		LOG.debug("Store : " + store);
		
		String itemParam = request.getParameter("item");
							
		//On récupère le compte
		Account account = (Account) request.getSession().getAttribute(ATT_SESSION_ACCOUNT);
		LOG.debug("Account : " + account);
		
		//On met à jour la boutique en fonction du compte
		store.updateAccount(account);
		
		//On ajoute le store dans la requete
		request.setAttribute(ATT_STORE, store);

		if(itemParam != null) {
			//On récupère l'item à partir de son identifiant
			Item item = (Item) store.getExperience(itemParam);
			LOG.debug("item : " + item);
			
			item.setAccount(account);
			LOG.debug("item : " + item);
			
			boolean isPossibleToAccess = item.isPossibleToAccess();
			LOG.debug(isPossibleToAccess);
			
			request.setAttribute(ATT_ISPOSSIBLETOACESS, isPossibleToAccess);
			
			//On ajoute le store dans la requete
			request.setAttribute(ATT_ITEM, item);
		}
			
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
}
