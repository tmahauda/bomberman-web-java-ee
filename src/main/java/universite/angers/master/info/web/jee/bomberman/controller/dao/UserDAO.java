package universite.angers.master.info.web.jee.bomberman.controller.dao;

import java.util.Collection;
import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * DAO User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class UserDAO extends DAO<User> {

	private static UserDAO userDAO;

	private UserDAO() {
		super(FactorySQLite.getInstance(), User.class);
	}

	public static synchronized UserDAO getInstance() {
		if (userDAO == null)
			userDAO = new UserDAO();

		return userDAO;
	}

	/**
	 * Rechercher par prénom
	 * 
	 * @param name
	 * @return
	 */
	public User getUserByFirstName(String firstName) {
		return this.searchByAttribute("firstName", firstName);
	}

	/**
	 * Rechercher par nom
	 * 
	 * @param name
	 * @return
	 */
	public User getUserByLastName(String lastName) {
		return this.searchByAttribute("lastName", lastName);
	}

	/**
	 * Rechercher par mail
	 * 
	 * @param email
	 * @return
	 */
	public User getUserByEmail(String email) {
		return this.searchByAttribute("email", email);

	}

	/**
	 * Rechercher par pseudo
	 * 
	 * @param pseudo
	 * @return
	 */
	public User getUserByPseudo(String pseudo) {
		return this.searchByAttribute("pseudo", pseudo);
	}

	/**
	 * Rechercher un utilisateur par attribut
	 * 
	 * @param name  le nom de l'attribut
	 * @param value la valeur à évaluer pour rechercher l'utilisateur
	 * @return
	 */
	private User searchByAttribute(String name, String value) {
		Collection<User> listuser = this.readAll(name, value);
		if (listuser != null && !listuser.isEmpty()) {
			// Il n'y a pas de méthode get() dans Collection. Par conséquent
			// il faut faire toArray[index] pour get un élement et le caster
			return (User) listuser.toArray()[0];
		} else {
			return null;
		}
	}
}