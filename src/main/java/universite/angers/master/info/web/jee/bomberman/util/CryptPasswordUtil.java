package universite.angers.master.info.web.jee.bomberman.util;

import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 * Classe utilitaire pour crypter un mot de passe
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CryptPasswordUtil {

	private CryptPasswordUtil() {

	}

	public static String cryptPass(String motPasse) {
		return BCrypt.withDefaults().hashToString(8, motPasse.toCharArray());
	}

	public static boolean checkPassword(String motPasse, String bcryptHash) {
		return BCrypt.verifyer().verify(motPasse.toCharArray(), bcryptHash).verified;
	}
}
