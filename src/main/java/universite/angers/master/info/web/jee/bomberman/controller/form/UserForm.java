package universite.angers.master.info.web.jee.bomberman.controller.form;

import universite.angers.master.info.web.jee.bomberman.controller.dao.UserDAO;
import universite.angers.master.info.web.jee.bomberman.util.CryptPasswordUtil;

/**
 * Formulaire qui permet de vérifier les données relatives à l'utilisateur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class UserForm {

	/**
	 * Vérifier si le pseudo utilisé existe déjà dans la BD
	 * 
	 * @param pseudo
	 * @return
	 */
	protected boolean existPseudoBdd(String pseudo) {
		return (UserDAO.getInstance().getUserByPseudo(pseudo) != null);
	}

	/**
	 * Vérifier si le mail utilisé existe déjà dans la BD
	 * 
	 * @param email
	 * @return
	 */
	protected boolean existEmailBdd(String email) {
		return (UserDAO.getInstance().getUserByEmail(email) != null);
	}

	/**
	 * Vérifier si le mot de passe d'un utilisateur est correct ou non
	 * 
	 * @param pseudo
	 * @param password
	 * @return
	 */
	protected boolean verifyPassword(String pseudo, String password) {
		if (UserDAO.getInstance().getUserByPseudo(pseudo) != null) {
			return CryptPasswordUtil.checkPassword(password,
					UserDAO.getInstance().getUserByPseudo(pseudo).getPassword());
		} else {
			return false;
		}
	}
}
