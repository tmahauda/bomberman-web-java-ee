package universite.angers.master.info.web.jee.bomberman.controller.dao;

import java.util.Collection;
import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.web.jee.bomberman.model.Account;
import universite.angers.master.info.web.jee.bomberman.model.Game;
import universite.angers.master.info.web.jee.bomberman.model.User;

/**
 * DAO Account
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AccountDAO extends DAO<Account> {

	private static AccountDAO accountDAO;

	private AccountDAO() {
		super(FactorySQLite.getInstance(), Account.class);
	}

	public static synchronized AccountDAO getInstance() {
		if (accountDAO == null)
			accountDAO = new AccountDAO();

		return accountDAO;
	}

	/**
	 * Rechercher un compte par prénom d'un utilisateur
	 * 
	 * @param firstName le prénom
	 * @return le compte associé
	 */
	public Account getAccountByFirstName(String firstName) {
		User user = UserDAO.getInstance().getUserByFirstName(firstName);
		return this.getAccountByUser(user);
	}

	/**
	 * Rechercher un compte par nom d'un utilisateur
	 * 
	 * @param lastName le nom
	 * @return le compte associé
	 */
	public Account getAccountByLastName(String lastName) {
		User user = UserDAO.getInstance().getUserByLastName(lastName);
		return this.getAccountByUser(user);
	}

	/**
	 * Rechercher un compte par email d'un utilisateur
	 * 
	 * @param email l'email
	 * @return le compte associé
	 */
	public Account getAccountByEmail(String email) {
		User user = UserDAO.getInstance().getUserByEmail(email);
		return this.getAccountByUser(user);
	}

	/**
	 * Rechercher un compte par pseudo d'un utilisateur
	 * 
	 * @param pseudo le pseudo
	 * @return le compte associé
	 */
	public Account getAccountByPseudo(String pseudo) {
		User user = UserDAO.getInstance().getUserByPseudo(pseudo);
		return this.getAccountByUser(user);
	}

	/**
	 * Rechercher un compte par utilisateur
	 * 
	 * @param user l'utilisateur
	 * @return le compte associé
	 */
	public Account getAccountByUser(User user) {
		Collection<Account> accounts = this.readAll("user", user);
		if (accounts != null && !accounts.isEmpty()) {
			// Il n'y a pas de méthode get() dans Collection. Par conséquent
			// il faut faire toArray[index] pour get un élement et le caster
			return (Account) accounts.toArray()[0];
		} else {
			return null;
		}
	}

	/**
	 * Supprimer un compte
	 * 
	 * @param ac le compte
	 * @return true si le compte est bien supprimé sinon false
	 */
	public boolean deleteAccount(Account ac) {
		if (ac == null)
			return false; // Précondition : je vérifie si le compte n'est pas null

		boolean userDelete = true;

		// On supprime d'abord l'utilisateur associé au compte si présent
		if (ac.getUser() != null) {
			userDelete = UserDAO.getInstance().delete(ac.getUser());
			
			//Puis ses parties associés
			if(!ac.getGamesLost().isEmpty()) {
				for(Game game : ac.getGamesLost())
					GameDAO.getInstance().delete(game);
			}
			
			if(!ac.getGamesWon().isEmpty()) {
				for(Game game : ac.getGamesWon())
					GameDAO.getInstance().delete(game);
			}
		}
		
		
		// Puis on supprime le compte
		boolean accountDelete = AccountDAO.getInstance().delete(ac);

		return userDelete && accountDelete;
	}
}
