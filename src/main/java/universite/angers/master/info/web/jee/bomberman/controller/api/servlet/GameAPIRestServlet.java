package universite.angers.master.info.web.jee.bomberman.controller.api.servlet;

import javax.servlet.annotation.WebServlet;

import universite.angers.master.info.web.jee.bomberman.controller.api.GameAPI;
import universite.angers.master.info.web.jee.bomberman.model.Game;

/**
 * API Game
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet(GameAPIRestServlet.URL)
public class GameAPIRestServlet extends APIRestServlet<Game> {

	private static final long serialVersionUID = 1L;
	protected static final String URL = "/api/game/*";

	public GameAPIRestServlet() {
		super(GameAPI.getInstance());
	}
}