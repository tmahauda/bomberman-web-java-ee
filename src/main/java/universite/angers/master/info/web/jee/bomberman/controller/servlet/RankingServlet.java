package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * Servlet qui permet de voir le classement
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/RankingServlet")
public class RankingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(RankingServlet.class);

	private static final String ATT_ACCOUNTS = "Accounts";
	private static final String VUE_DAILY = "/WEB-INF/jsp/ranking.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RankingServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Collection<Account> accounts = AccountDAO.getInstance().readAll();
		LOG.debug("Accounts : " + accounts);

		request.setAttribute(ATT_ACCOUNTS, accounts);

		this.getServletContext().getRequestDispatcher(VUE_DAILY).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
}
