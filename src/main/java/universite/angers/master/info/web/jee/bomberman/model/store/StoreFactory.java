package universite.angers.master.info.web.jee.bomberman.model.store;

import java.util.Arrays;

import universite.angers.master.info.web.jee.bomberman.model.Rank;

/**
 * Fabrique qui permet de construire le magasin Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class StoreFactory {

	private static Store store;

	private StoreFactory() {

	}

	/**
	 * Singleton : un seul magasin du Bomberman
	 * 
	 * @return
	 */
	public static Store getInstance() {
		if (store == null)
			store = buildStoreBomberman();

		return store;
	}

	/**
	 * Construit un magasin pour améliorer l'expérience du jeu Bomberman
	 * 
	 * @return
	 */
	private static Store buildStoreBomberman() {
		Store store = new Store();

		/**
		 * LES COULEURS
		 */

		Category colors = new Category("CAT_COLOR", "Couleurs", "", "Les couleur de Bomberman", "");

		Item colorBlue = new Item("ITEM_COLOR_BLUE", "Couleur bleue", "Déjà possédé par défaut", "Your Bomberman is in blue color. Also some things in the game change in relation to the choosen color.", "bomberman blue.jpeg");
		colorBlue.setPrice(0);
		colorBlue.setRanks(Arrays.asList(Rank.values()));
		
		
		Item colorRed = new Item("ITEM_COLOR_RED", "Couleur rouge", "Avoir au minimum 10 points", "Your Bomberman will be in red color. Also some things in the game may also be change in relation to the choosen color.", "bomberman_red.png");
		colorRed.setPrice(10);
		colorRed.setRanks(Arrays.asList(Rank.values()));

		Item colorGreen = new Item("ITEM_COLOR_GREEN", "Couleur verte", "Avoir au minimum 10 points", "Your Bomberman will be in green color. Also some things in the game may also be change in relation to the choosen color.", "bomberman green.png");
		colorGreen.setPrice(10);
		colorGreen.setRanks(Arrays.asList(Rank.values()));

		Item colorWhite = new Item("ITEM_COLOR_WHITE", "Couleur blanche", "Avoir au minimum 10 points", "Your Bomberman will be in white color. Also some things in the game may also be change in relation to the choosen color.", "bomberman white.png");
		colorWhite.setPrice(10);
		colorWhite.setRanks(Arrays.asList(Rank.values()));

		colors.addExperience(colorBlue);
		colors.addExperience(colorRed);
		colors.addExperience(colorGreen);
		colors.addExperience(colorWhite);
				

		/**
		 * LES MAPS
		 */

		Category maps = new Category("CAT_MAP", "Maps", "", "les differentes maps - Layout", "");

		Item mapLevel1 = new Item("ITEM_MAP_LEVEL1", "Niveau 1", "Déjà possédé par défaut", "Continuing through the Green Village east of his home, Peace Town, White Bomberman moves onto Levels 2, which all take place in Black Bomberman's castle after it was taken over by more enemies.", "level.png");
		mapLevel1.setPrice(0);
		mapLevel1.setRanks(Arrays.asList(Rank.values()));

		Item mapLevel2 = new Item("ITEM_MAP_LEVEL2", "Niveau 2", "Avoir au minimum 200 points et au minimum le rang Bronze", "Continuing through the Green Village east of his home, Peace Town, White Bomberman moves onto Levels 3, which all take place in Black Bomberman's castle after it was taken over by more enemies.", "level.png");
		mapLevel2.setPrice(200);
		mapLevel2.setRanks(
				Arrays.asList(Rank.BRONZE, Rank.SILVER, Rank.GOLD, Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		Item mapLevel3 = new Item("ITEM_MAP_LEVEL3", "Niveau 3", "Avoir au minimum 300 points et au minimum le rang Silver", "Continuing through the Green Village east of his home, Peace Town, White Bomberman moves onto Levels 3+, which all take place in Black Bomberman's castle after it was taken over by more enemies.", "level.png");
		mapLevel3.setPrice(300);
		mapLevel3.setRanks(Arrays.asList(Rank.SILVER, Rank.GOLD, Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		maps.addExperience(mapLevel1);
		maps.addExperience(mapLevel2);
		maps.addExperience(mapLevel3);

		/**
		 * LES SOLUTIONS
		 */

		Category soluces = new Category("CAT_SOLUCE", "Solutions", "", "les niveaux avec leur solution", "");

		Item soluceLevel1 = new Item("ITEM_SOLUCE_LEVEL1", "Niveau 1", "Avoir au minimum 100 points et au minimum le rang Gold", "the solution of level 1, that's make you to pass through next level, and more technical moves", "level.png");
		soluceLevel1.setPrice(100);
		soluceLevel1.setRanks(Arrays.asList(Rank.GOLD, Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		Item soluceLevel2 = new Item("ITEM_SOLUCE_LEVEL2", "Niveau 2", "Avoir au minimum 200 points et au minimum le rang Platinium", "the solution of level 2, that's make you to pass through next level, and more technical moves", "level.png");
		soluceLevel2.setPrice(200);
		soluceLevel2.setRanks(Arrays.asList(Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		Item soluceLevel3 = new Item("ITEM_SOLUCE_LEVEL3", "Niveau 3", "Avoir au minimum 300 points et au minimum le rang Diamond", "the solution of level 3, that's make you to pass through next level, and more technical moves", "level.png");
		soluceLevel3.setPrice(300);
		soluceLevel3.setRanks(Arrays.asList(Rank.DIAMOND, Rank.CHAMPION));

		soluces.addExperience(mapLevel1);
		soluces.addExperience(mapLevel2);
		soluces.addExperience(mapLevel3);

		/**
		 * LES BONUS
		 */

		Category bonus = new Category("CAT_BONUS", "Bonus", "", "les differentes bonus", "");

		Item bonusSpeed = new Item("ITEM_BONUS_SPEED", "Speed", "Avoir au minimum 100 points et au minimum le rang Champion", "The Skate power-up (also known as Speed Up or Roller-Shoes) is a recurring item in the Bomberman series. It increases the player's speed by 1.", "speed.png");
		bonusSpeed.setPrice(100);
		bonusSpeed.setRanks(Arrays.asList(Rank.CHAMPION));

		bonus.addExperience(bonusSpeed);

		// BOMB

		Category bonusBomb = new Category("CAT_BONUS_BOMB", "Bonus bombe", "", "Bonus de bombe", "");

		Item bonusBombFire = new Item("ITEM_BONUS_BOMB_FIRE", "Bombe Fire", "Avoir au minimum 100 points et au minimum le rang Silver", "The Full Fire item is an advanced version of the Fire item. It maxes out the player's fire capacity, making his or her bombs explode with the biggest possible blast radius. Usually, this extends over a good portion of the battlefield.", "Full_Fire.png");
		bonusBombFire.setPrice(100);
		bonusBombFire.setRanks(Arrays.asList(Rank.SILVER, Rank.GOLD, Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		Item bonusBombPut = new Item("ITEM_BONUS_BOMB_PUT", "Bombe Put", "Avoir au minimum 100 points et au minimum le rang Gold", "The Bomb powerup is a recurring item in the Bomberman series. It increases the player's amount of bombs by one. Bomb Capacity is the amount of bombs a player can have on screen at the same time.", "Bomb_Up.png");
		bonusBombPut.setPrice(100);
		bonusBombPut.setRanks(Arrays.asList(Rank.GOLD, Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		bonusBomb.addExperience(bonusBombFire);
		bonusBomb.addExperience(bonusBombPut);
		bonus.addExperience(bonusBomb);

		// LIFE

		Category bonusLife = new Category("CAT_BONUS_LIFE", "Bonus vie", "", "Bonus de vie", "");

		Item bonusLifeFireSuit = new Item("ITEM_BONUS_LIFE_FIRE_SUIT", "Vie fire suit", "Avoir au minimum 100 points et au minimum le rang Platinium", "The 1-Up item is an item that has a picture of Bomberman or a heart on it. It grants the player an additional try at the stage in the event that they die. This type of item is a common and widespread type in video games, especially exclusively single player games.", "1-Up.png");
		bonusLifeFireSuit.setPrice(100);
		bonusLifeFireSuit.setRanks(Arrays.asList(Rank.PLATINIUM, Rank.DIAMOND, Rank.CHAMPION));

		Item bonusLifeHeart = new Item("ITEM_BONUS_LIFE_HEART", "Vie heart", "Avoir au minimum 100 points et au minimum le rang Diamond", "The Heart (sometimes called the Life-Up or Revive) is an item that appears in many Bomberman games, starting with the very first game in the series. It allows the player to survive one bomb blast, enemy attack, or damaging trap while it is in his or her possession. The maximum amount of Hearts that the player is allowed to carry varies from game to game.", "Heart_Art.png");
		bonusLifeHeart.setPrice(100);
		bonusLifeHeart.setRanks(Arrays.asList(Rank.DIAMOND, Rank.CHAMPION));

		bonusLife.addExperience(bonusLifeFireSuit);
		bonusLife.addExperience(bonusLifeHeart);
		bonus.addExperience(bonusLife);

		store.addExperience(colors);
		store.addExperience(maps);
		store.addExperience(soluces);
		store.addExperience(bonus);

		return store;
	}
}
