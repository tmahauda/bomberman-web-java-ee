package universite.angers.master.info.web.jee.bomberman.controller.api.servlet;

import javax.servlet.annotation.WebServlet;
import universite.angers.master.info.web.jee.bomberman.controller.api.StructureAPI;
import universite.angers.master.info.web.jee.bomberman.model.cours.Structure;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet(StructureAPIRestServlet.URL)
public class StructureAPIRestServlet extends APIRestServlet<Structure> {

	private static final long serialVersionUID = 1L;
	protected static final String URL = "/api/structure/*";

	public StructureAPIRestServlet() {
		super(StructureAPI.getInstance());
	}
}