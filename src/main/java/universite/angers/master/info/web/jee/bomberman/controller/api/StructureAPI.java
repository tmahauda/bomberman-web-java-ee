package universite.angers.master.info.web.jee.bomberman.controller.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.web.jee.bomberman.controller.dao.StructureDAO;
import universite.angers.master.info.web.jee.bomberman.model.cours.Structure;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class StructureAPI {

	private static APIRestStringToObject<Structure> structAPI;

	private StructureAPI() {

	}

	public static synchronized APIRestStringToObject<Structure> getInstance() {
		if (structAPI == null) {
			structAPI = APIRestFactory.getAPIRestObjectToJson(Charset.UTF_8, StructureDAO.getInstance(),
					new TypeToken<Structure>() {}, new TypeToken<Collection<Structure>>() {});
		}

		return structAPI;
	}
}