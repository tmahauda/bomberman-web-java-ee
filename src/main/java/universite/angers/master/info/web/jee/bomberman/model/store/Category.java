package universite.angers.master.info.web.jee.bomberman.model.store;

import java.util.HashMap;
import java.util.Map;

/**
 * Catégorie qui permet de regrouper un ensemble d'items à débloquer
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Category extends Experience {

	private Map<String, Experience> experiences;
	
	public Category() {
		this("", "", "", "", "");
	}
	
	public Category(String identifiant, String name, String condition, String description, String pathPicture) {
		super(identifiant, name, condition, description, pathPicture);
		this.experiences = new HashMap<>();
	}

	@Override
	public boolean isPossibleToAccess() {
		//Si au moins une expérience est accessible dans ce cas la catégorie est accessible
		for(Experience experience : experiences.values()) {
			if(experience.isPossibleToAccess()) return true;
		}
		
		return false;
	}

	@Override
	public String display() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<div class=\"title\">");
		sb.append("<i class=\"dropdown icon\"></i>");
		sb.append(this.name);
		sb.append("</div>");
		sb.append("<div class=\"content\">");
		sb.append(this.description);
		sb.append("<div class=\"accordion transition visible\" style=\"display: block !important;\">");
		
		for(Experience experience : experiences.values()) {
			sb.append(experience.display());
			sb.append("\n");
		}
		
		sb.append("</div>");
		sb.append("</div>");
		
		return sb.toString();
	}
	
	/**
	 * Ajouter une expérience (sous-catégorie ou item) dans la catégorie
	 * @param experience
	 * @return
	 */
	public boolean addExperience(Experience experience) {
		if(this.experiences.containsKey(experience.getIdentifiant())) return false;
		
		this.experiences.put(experience.getIdentifiant(), experience);
		return true;
	}

	/**
	 * @return the experiences
	 */
	public Map<String, Experience> getExperiences() {
		return experiences;
	}

	/**
	 * @param experiences the experiences to set
	 */
	public void setExperiences(Map<String, Experience> experiences) {
		this.experiences = experiences;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((experiences == null) ? 0 : experiences.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (experiences == null) {
			if (other.experiences != null)
				return false;
		} else if (!experiences.equals(other.experiences))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Category [experiences=" + experiences + ", account=" + account + ", identifiant=" + identifiant
				+ ", name=" + name + ", condition=" + condition + ", description=" + description + ", pathPicture="
				+ pathPicture + "]";
	}
}
