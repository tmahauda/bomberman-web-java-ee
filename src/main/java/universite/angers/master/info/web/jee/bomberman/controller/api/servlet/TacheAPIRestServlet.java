package universite.angers.master.info.web.jee.bomberman.controller.api.servlet;

import javax.servlet.annotation.WebServlet;
import universite.angers.master.info.web.jee.bomberman.controller.api.TacheAPI;
import universite.angers.master.info.web.jee.bomberman.model.cours.Tache;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet(TacheAPIRestServlet.URL)
public class TacheAPIRestServlet extends APIRestServlet<Tache> {

	private static final long serialVersionUID = 1L;
	protected static final String URL = "/api/tache/*";

	public TacheAPIRestServlet() {
		super(TacheAPI.getInstance());
	}
}