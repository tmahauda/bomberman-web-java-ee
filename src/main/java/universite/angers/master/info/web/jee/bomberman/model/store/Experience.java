package universite.angers.master.info.web.jee.bomberman.model.store;

import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * Expérience qui permet d'offrir des contenus supplémentaires au jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class Experience {
	
	/**
	 * Le compte courant qui consulte l'expérience
	 */
	protected Account account;
	
	/**
	 * L'identifiant de l'expérience
	 */
	protected String identifiant;
	
	/**
	 * Le nom de l'expérience
	 */
	protected String name;
	
	/**
	 * La condition pour accéder à l'expérience : comment faire pour la déploquer ?
	 */
	protected String condition;
	
	/**
	 * La description de l'expérience : elle propose quoi ?
	 */
	protected String description;
	
	/**
	 * L'image qui représente l'expérience : elle doit être stocké dans le dossier public pour accéder depuis HTML
	 */
	protected String pathPicture;
	
	public Experience() {
		this("", "", "", "", "");
	}
	
	public Experience(String identifiant, String name, String condition, String description, String pathPicture) {
		this.identifiant = identifiant;
		this.name = name;
		this.condition = condition;
		this.description = description;
		this.pathPicture = pathPicture;
	}

	/**
	 * Vérifier si on peut accéder à l'expérience. Voir l'attribut condition pour savoir comment y accéder
	 * @return
	 */
	public abstract boolean isPossibleToAccess();
	
	/**
	 * Afficher l'expérience
	 * @param indent
	 * @return
	 */
	public abstract String display();
	
	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * @return the identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the pathPicture
	 */
	public String getPathPicture() {
		return pathPicture;
	}

	/**
	 * @param pathPicture the pathPicture to set
	 */
	public void setPathPicture(String pathPicture) {
		this.pathPicture = pathPicture;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((condition == null) ? 0 : condition.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((identifiant == null) ? 0 : identifiant.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pathPicture == null) ? 0 : pathPicture.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Experience other = (Experience) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (condition == null) {
			if (other.condition != null)
				return false;
		} else if (!condition.equals(other.condition))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (identifiant == null) {
			if (other.identifiant != null)
				return false;
		} else if (!identifiant.equals(other.identifiant))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pathPicture == null) {
			if (other.pathPicture != null)
				return false;
		} else if (!pathPicture.equals(other.pathPicture))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Experience [account=" + account + ", identifiant=" + identifiant + ", name=" + name + ", condition="
				+ condition + ", description=" + description + ", pathPicture=" + pathPicture + "]";
	}
}
