package universite.angers.master.info.web.jee.bomberman.controller.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.web.jee.bomberman.controller.dao.TacheDAO;
import universite.angers.master.info.web.jee.bomberman.model.cours.Tache;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TacheAPI {

	private static APIRestStringToObject<Tache> tacheAPI;

	private TacheAPI() {

	}

	public static synchronized APIRestStringToObject<Tache> getInstance() {
		if (tacheAPI == null) {
			tacheAPI = APIRestFactory.getAPIRestObjectToJson(Charset.UTF_8, TacheDAO.getInstance(),
					new TypeToken<Tache>() {}, new TypeToken<Collection<Tache>>() {});
		}

		return tacheAPI;
	}
}