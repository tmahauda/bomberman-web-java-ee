package universite.angers.master.info.web.jee.bomberman.controller.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestStringToObject;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.web.jee.bomberman.controller.dao.CoursDAO;
import universite.angers.master.info.web.jee.bomberman.model.cours.Cours;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CoursAPI {

	private static APIRestStringToObject<Cours> coursAPI;

	private CoursAPI() {

	}

	public static synchronized APIRestStringToObject<Cours> getInstance() {
		if (coursAPI == null) {
			coursAPI = APIRestFactory.getAPIRestObjectToJson(Charset.UTF_8, CoursDAO.getInstance(),
					new TypeToken<Cours>() {}, new TypeToken<Collection<Cours>>() {});
		}

		return coursAPI;
	}
}