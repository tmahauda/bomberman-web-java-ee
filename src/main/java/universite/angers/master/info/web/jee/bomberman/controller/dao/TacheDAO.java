package universite.angers.master.info.web.jee.bomberman.controller.dao;

import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.web.jee.bomberman.model.cours.Tache;

/**
 * DAO Account
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TacheDAO extends DAO<Tache> {

	private static TacheDAO tacheDAO;

	private TacheDAO() {
		super(FactorySQLite.getInstance(), Tache.class);
	}

	public static synchronized TacheDAO getInstance() {
		if (tacheDAO == null)
			tacheDAO = new TacheDAO();

		return tacheDAO;
	}
}
