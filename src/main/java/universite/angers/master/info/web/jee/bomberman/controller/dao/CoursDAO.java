package universite.angers.master.info.web.jee.bomberman.controller.dao;

import universite.angers.master.info.orm.bd.jdbc.sqlite.FactorySQLite;
import universite.angers.master.info.orm.dao.DAO;
import universite.angers.master.info.web.jee.bomberman.model.cours.Cours;

/**
 * DAO Account
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CoursDAO extends DAO<Cours> {

	private static CoursDAO coursDAO;

	private CoursDAO() {
		super(FactorySQLite.getInstance(), Cours.class);
	}

	public static synchronized CoursDAO getInstance() {
		if (coursDAO == null)
			coursDAO = new CoursDAO();

		return coursDAO;
	}
}
