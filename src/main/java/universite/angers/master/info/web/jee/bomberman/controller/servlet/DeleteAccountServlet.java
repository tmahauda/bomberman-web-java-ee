package universite.angers.master.info.web.jee.bomberman.controller.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import universite.angers.master.info.web.jee.bomberman.controller.dao.AccountDAO;
import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * Servlet qui permet de supprimer un compte
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet("/DeleteAccountServlet")
public class DeleteAccountServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(DeleteAccountServlet.class);

	private static final String URL_REDIRECTION = "http://localhost:8080/project-web-jee-bomberman/HomeServlet";
	private static final String ATT_SESSION_ACCOUNT = "sessionCompteUtilisateur";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteAccountServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		LOG.debug("Session    :" + session);

		Account sessionAccount = (Account) session.getAttribute(ATT_SESSION_ACCOUNT);
		LOG.debug("Objet stocké dans la session     :" + sessionAccount);

		boolean delete = AccountDAO.getInstance().deleteAccount(sessionAccount);
		LOG.debug("Compte supprime ? " + delete);

		if (delete) {
			/* Redirection vers la page d'accueil ! */
			LOG.debug("Compte supprimer");
			try {
				response.sendRedirect(URL_REDIRECTION);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		} else {
			LOG.debug("Impossible de supprimer le compte");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
}
