package universite.angers.master.info.web.jee.bomberman.controller.api.servlet;

import javax.servlet.annotation.WebServlet;

import universite.angers.master.info.web.jee.bomberman.controller.api.AccountAPI;
import universite.angers.master.info.web.jee.bomberman.model.Account;

/**
 * API Account
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@WebServlet(AccountAPIRestServlet.URL)
public class AccountAPIRestServlet extends APIRestServlet<Account> {

	private static final long serialVersionUID = 1L;
	protected static final String URL = "/api/account/*";

	public AccountAPIRestServlet() {
		super(AccountAPI.getInstance());
	}
}