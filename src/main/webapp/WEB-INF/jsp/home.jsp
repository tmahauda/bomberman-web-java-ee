<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="UTF-8">	
	<link href="imports/Bomberman-icon.png" rel="icon" sizes="128x128">
	<title>Page d'accueil</title>
	<!-- <link rel="import" href="imports/head_css_js.html"> -->
	<link rel="stylesheet" type="text/css" href="Semantic/components/myStyle.css">
	<link rel="stylesheet" href="Semantic/components/stylehome.css">
	
	<link rel="stylesheet" type="text/css" href="Semantic/components/reset.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/site.css">
	
	<link rel="stylesheet" type="text/css" href="Semantic/components/container.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/grid.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/header.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/image.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/menu.css">
	
	<link rel="stylesheet" type="text/css" href="Semantic/components/divider.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/segment.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/form.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/input.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/button.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/list.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/message.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/myStyle.css">
	<link rel="stylesheet" type="text/css" href="Semantic/components/icon.css">
		
	<script src="Semantic/components/form.js"></script>
	<script src="Semantic/components/transition.js"></script>
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.css'> 
	
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	
	<link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />
	<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
	<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>


  <script>
       $(function(){
           $("#logo").load("imports/logo.html");
       });
   </script>
</head>
<body>
<!-- partial:index.partial.html -->
<div class="ui sidebar vertical left menu overlay visible" style="-webkit-transition-duration: 0.1s; overflow: visible !important;">
  <div class="item logo">
    <!-- <div id="logo"></div> -->
    <img src="http://icons.iconarchive.com/icons/yellowicon/game-stars/128/Bomberman-icon.png" />
    <img src="http://icons.iconarchive.com/icons/yellowicon/game-stars/128/Bomberman-icon.png" style="display: none" />
  </div>
	

</div>
<div class="pusher">
  <div class="ui menu asd borderless" style="border-radius: 0!important; border: 0; margin-left: 260px; -webkit-transition-duration: 0.1s;">
  
    <a class="item">Jeu de Bomberman</a>
    <div class="right menu">      
      <form method="get" action="InscriptionServlet">
      	<div class="item">                        
        	<input type="submit" value="S'inscrire" class="ui primary button" />
        </div>
      </form>
      <form method="get" action="ConnexionServlet">
	      <div class="item">	          	        	       
	        <input type="submit" value="Se connecter" class="ui primary button" />        
	      </div>
      </form>
    </div>
  </div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.js'></script>
<script src="Semantic/components/scripthome.js"></script>

</body>
</html>
    